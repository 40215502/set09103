from flask import Flask, render_template, url_for, request
import sqlite3
app = Flask(__name__)

@app.route("/")
def root():
    return render_template('wedding.html'), 200
	
@app.route("/add/")
def add():
    return render_template('add.html'), 200	

@app.route("/addwedding/",methods = ['POST', 'GET'])
def addwedding():
    if request.method == 'POST':
      db = './var/sqlite3.db'
      conn = sqlite3.connect(db)
      try:
         p1 = request.form['p1']
         p2 = request.form['p2']
         date = request.form['date']
         cur = conn.cursor()
         cur.execute("INSERT INTO weddings2016 (partner1,partner2,date) VALUES (?,?,?)",(p1,p2,date) )
			
         conn.commit()
         msg = "wedding successfully added"
      except:
         conn.rollback()
         msg =  "error in adding wedding"
      finally:
         return render_template('result.html', msg = msg)
         con.close()


@app.route("/bookings/")
def list():
   conn = sql.connect("var/sqlite3.db")
   conn.row_factory = sql.Row
   
   cur = conn.cursor()
   cur.execute("select * from weddings2106")
   
   rows = cur.fetchall();
   return render_template("weddingslist.html",rows = rows)	

if __name__ == "__main__":
    app.run(host="0.0.0.0")
