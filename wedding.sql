DROP TABLE if EXISTS weddings;

CREATE TABLE weddings (
  partner1 text,
  partner2 text,
  wedding_date text
);
